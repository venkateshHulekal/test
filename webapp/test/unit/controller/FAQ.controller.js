/*global QUnit*/

sap.ui.define([
	"com/capgemini/coe/FAQ/controller/FAQ.controller"
], function (Controller) {
	"use strict";

	QUnit.module("FAQ Controller");

	QUnit.test("I should test the FAQ controller", function (assert) {
		var oAppController = new Controller();
		oAppController.onInit();
		assert.ok(oAppController);
	});

});