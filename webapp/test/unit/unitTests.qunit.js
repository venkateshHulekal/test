/* global QUnit */
QUnit.config.autostart = false;

sap.ui.getCore().attachInit(function () {
	"use strict";

	sap.ui.require([
		"com/capgemini/coe/FAQ/test/unit/AllTests"
	], function () {
		QUnit.start();
	});
});